// Sriramajayam

#include <regstr_Pose.h>
#include <gsl/gsl_sf_bessel.h>
#include <cmath>
#include <cassert>

namespace regstr
{
  // Default constructor.
  // Sets rotation to the identity and translation to zero
  Pose::Pose()
  {
    for(int i=0; i<3; ++i)
      {
	tvec[i] = 0.;
	for(int j=0; j<3; ++j)
	  Rot[i][j] = 0.;
	Rot[i][i] = 1.;
      }
  }

  // Copy constructor, copy the state
  Pose::Pose(const Pose& obj)
  {
    for(int i=0; i<3; ++i)
      {
	tvec[i] = obj.tvec[i];
	for(int j=0; j<3; ++j)
	  Rot[i][j] = obj.Rot[i][j];
      }
  }
    
  // Assignment
  Pose& Pose::operator=(const Pose& rhs)
  {
    if(&rhs==this) return *this;
    for(int i=0; i<3; ++i)
      {
	tvec[i] = rhs.tvec[i];
	for(int j=0; j<3; ++j)
	  Rot[i][j] = rhs.Rot[i][j];
      }
    return *this;
  }
	
  // Set the pose
  void Pose::SetPose(const double R[][3], const double* t)
  {
    for(int i=0; i<3; ++i)
      {
	tvec[i] = t[i];
	for(int j=0; j<3; ++j)
	  Rot[i][j] = R[i][j];
      }
    // Check that the rotation is unitary
    assert(std::abs(Det(Rot)-1.)<1.e-4 &&
	   "regstr::Pose::SetPose- matrix not a rotation");
    return;
  }
    
  // Returns the pose
  void Pose::GetPose(double R[][3], double* t) const
  {
    for(int i=0; i<3; ++i)
      {
	t[i] = tvec[i];
	for(int j=0; j<3; ++j)
	  R[i][j] = Rot[i][j];
      }
    return;
  }
      
  // Update point
  void Pose::Update(const double* theta, const double* dt)
  {
    // Upate the translation
    for(int i=0; i<3; ++i)
      tvec[i] += dt[i];

    // Update the rotation: R <- exp(HodgeStar(theta))R
    HodgeStar(theta, skw);
    ExpSO3(skw, expMat);
    for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
	{
	  tMat[i][j] = 0.;
	  for(int k=0; k<3; ++k)
	    tMat[i][j] += expMat[i][k]*Rot[k][j];
	}
    for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
	Rot[i][j] = tMat[i][j];
    return;
  }
    
  // Compute the skew matrix for a given axial vector
  void Pose::HodgeStar(const double* theta, double skw[][3])
  {
    skw[0][0] = 0.;        skw[0][1] = -theta[2]; skw[0][2] = theta[1];
    skw[1][0] = theta[2];  skw[1][1] = 0.;        skw[1][2] = -theta[0];
    skw[2][0] = -theta[1]; skw[2][1] = theta[0];  skw[2][2] = 0.;
    return;
  }
    
  // Compute the exponential map for a given skew matrix
  void Pose::ExpSO3(const double skw[][3], double Mat[][3])
  {
    // I + sinc(x) Skw + (1-cos(x))/x^2 Skw^2
    // = I + sinc(x) skw + (1/2) sinc(x/2)^2 skw^2
    
    // Angle
    double angle = sqrt(skw[2][1]*skw[2][1] +
			skw[0][2]*skw[0][2] +
			skw[1][0]*skw[1][0]);
    
    double sinc = gsl_sf_bessel_j0(angle);
    double sincby2sq = gsl_sf_bessel_j0(angle/2.);
    sincby2sq *= sincby2sq;
    
    for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
	{
	  Mat[i][j] = 0.;
	  for(int k=0; k<3; ++k)
	    Mat[i][j] += skw[i][k]*skw[k][j];
	  Mat[i][j] *= 0.5*sincby2sq;
	  Mat[i][j] += sinc*skw[i][j];
	}
    Mat[0][0] += 1.;
    Mat[1][1] += 1.;
    Mat[2][2] += 1.;
    return;
  }
  
  // Compute the determinant of a matrix
  double Pose::Det(const double Mat[][3])
  {
    double det = 0.;
    for(int i=0; i<3; ++i)
      det += Mat[0][i]*(Mat[1][(i+1)%3]*Mat[2][(i+2)%3]-
			Mat[1][(i+2)%3]*Mat[2][(i+1)%3]);
    return det;
  }
  
}
