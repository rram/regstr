// Sriramajayam

#ifndef REGSTR_POSE_MATCHER_H
#define REGSTR_POSE_MATCHER_H

#include <regstr_Pose.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_eigen.h>

namespace regstr
{
  //! Workspace of resolving a linear system of equations
  //! required in the class PoseMatcher
  struct PoseMatcherWorkspace
  {
    gsl_vector* rVec; // residual vector, used during assembly
    gsl_matrix* kMat; // Stiffness, used during assembly
    gsl_permutation* perm; // Permutation used during linear solves
    gsl_vector* inc; // solution- configuration increments

    // For eigenvalue & eigencvector calculations
    gsl_eigen_nonsymmv_workspace* eigWrkSpc;
    gsl_vector_complex* eigVals;
    gsl_matrix_complex* eigVecs;
      
    // Constructor: allocate memory for gsl structure
    PoseMatcherWorkspace(); 
    // Destructor: deallocate memory for gsl structures
    ~PoseMatcherWorkspace();
    // Disable copy and assignment
    PoseMatcherWorkspace(const PoseMatcherWorkspace&) = delete;
    PoseMatcherWorkspace& operator=(const PoseMatcherWorkspace&) = delete;
  };
    
    
  //! Abstract base class for computing isometries to
  //! match point clouds with other point clouds/surfaces.
  class PoseMatcher
  {
  public:
    //! default constructor, does nothing
    inline PoseMatcher() {}

    //! Default destructor, does nothing
    inline virtual ~PoseMatcher() {}

    //! Copy constructor: copy the pose
    inline PoseMatcher(const PoseMatcher& obj)
      :pose(obj.pose) {}

    //! Disable assignment
    PoseMatcher& operator=(const PoseMatcher&) = delete;
      
    //! Returns the pose
    inline void GetPose(double R[][3], double* t) const
    { pose.GetPose(R, t); return; }

    inline const Pose& GetPose() const
    { return pose; }
      
    //! Sets the pose
    inline void SetPose(const double R[][3], const double* t)
    { pose.SetPose(R, t); return; }

    //! Main functionality: compute the error functional.
    //! Abstract
    virtual double ComputeError() const = 0;

    //! Main functionality: compute the linearizations of the error functional
    //! Abstract
    virtual void GetLinearizations(double* res, double* stiff) const = 0;
      
    //! Main functionality: Iterate
    //! \param[in] factor Scaling factor for increments
    //! Returns the norm of the residual and the solution increment computed
    virtual void Iterate(const double factor, double& solNorm, double& resNorm);

    //! Main functionality: Iterate with specific dofs set to zero.
    //! \param[in] factor Scaling factor for increments
    //! Returns the norm of the residual and the solution increment computed
    virtual void Iterate(const double factor, const int nzerodofs, const int* zerodofs,
			 double& solNorm, double& resNorm);

    //! Computes the eigenvalues at the current pose
    void GetEigenvalues(double* eigvals) const;
      
  private:
    Pose pose;
    mutable PoseMatcherWorkspace ws;
  };
}

#endif
