// Sriramajayam

#include <regstr_PointCloudPoseMatcher.h>
#include <cassert>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace regstr
{
  // Constructor
  PointCloudPoseMatcher::PointCloudPoseMatcher(const PointCloud& rpc, const PointCloud& ppc,
					       const int nMaxThreads)
    :PoseMatcher(), refPC(rpc), pertPC(ppc)
  {
    // Allocate memory for thread-local workspaces
    assert(nMaxThreads>=1 && "regstr::PointCloudPoseMatcher- Unexpected number of threads");
#ifndef _OPENMP
    assert(nMaxThreads==1 && "regstr::PointCloudPoseMatcher- use 1 thread without openmp");
#endif
    WrkSpc.resize(nMaxThreads);
  }

    
  // Access the thread-local workspace for assembly
  PCPoseMatcherWorkspace& PointCloudPoseMatcher::GetLocalWorkspace() const
  {
    // Get this thread number
    int thrnum = 0;
#ifdef _OPENMP
    thrnum = omp_get_thread_num();
    assert(thrnum<static_cast<int>(WrkSpc.size()) &&
	   "regstr::PointCloudPoseMatcher::GetLocalWorkspace- Exceeded max-number of threads");
#endif
    return WrkSpc[thrnum];
  }
    
    
  // Main functionality:
  // Computes the error at the current state
  double PointCloudPoseMatcher::ComputeError() const
  {
    // Access the pose of the point cloud
    double Rot[3][3], tvec[3];
    GetPose().GetPose(Rot, tvec);
      
#pragma omp parallel default(shared)
    {
      // Get the local error measure
      auto& ws = GetLocalWorkspace();
      auto& Err = ws.Err;
      Err = 0.;
      auto* Y = ws.vec1; // Closest point in the reference cloud
      auto* W = ws.vec2; // Rx+t
	
#pragma omp for
      for(int p=0; p<pertPC.nPoints; ++p)
	{
	  // This local point
	  const auto* X = &pertPC.points[3*p];

	  // W = Rx+t
	  for(int i=0; i<3; ++i)
	    {
	      W[i] = tvec[i];
	      for(int j=0; j<3; ++j)
		W[i] += Rot[i][j]*X[j];
	    }

	  // Get the corresponding point in the master cloud
	  GetCorrespondence(p, W, Y);

	  // Update the error on this thread
	  for(int k=0; k<3; ++k)
	    Err += 0.5*(W[k]-Y[k])*(W[k]-Y[k]);
	}
    }

    // Sum contributions for each thread
    double Err = 0;
    for(auto& ws:WrkSpc)
      Err += ws.Err;
    return Err;
  }
      
  // Main functionality
  // Compute the residual and its linearization
  void PointCloudPoseMatcher::
  GetLinearizations(double* res, double* stiff) const
  {
    // Access this pose
    double Rot[3][3], tvec[3];
    GetPose().GetPose(Rot, tvec);

    // Useful for computing cross products
    const int next[] =  {1,2,0};
    const int nnext[] = {2,0,1};
      
#pragma omp parallel default(shared)
    {
      // Access the local workspace
      auto& ws = GetLocalWorkspace();
      auto* Y = ws.vec1; // closest point
      auto* W = ws.vec2; // Rx
      auto* tmY = ws.vec3; // t-Y
      auto* pertPt = ws.vec4; // Rx+t
      auto* skW = ws.mat1; // skw(W)
      auto* skT = ws.mat2; // skw(tvec)
      auto* skT_skW = ws.mat3; // skw(tvec)*skw(W)
      auto* rVec = ws.rVec; // residual
      auto* kMat = ws.kMat; // stiffness

      // Initialize the residual & stiffness to zero on this thread
      for(int i=0; i<6; ++i)
	{
	  rVec[i] = 0.;
	  for(int j=0; j<6; ++j)
	    kMat[i][j] = 0.;
	}
	
#pragma omp for
      for(int p=0; p<pertPC.nPoints; ++p)
	{
	  // Access this point
	  const auto* X = &pertPC.points[3*p];
	    
	  // Compute W = Rx
	  for(int i=0; i<3; ++i)
	    {
	      W[i] = 0.;
	      for(int j=0; j<3; ++j)
		W[i] += Rot[i][j]*X[j];
	    }

	  // pertPt = Rx+t
	  for(int i=0; i<3; ++i)
	    pertPt[i] = W[i]+tvec[i];
	    
	  // Identify the closest point to pertPt
	  GetCorrespondence(p, pertPt, Y);
	    
	  // tvec-Y
	  for(int i=0; i<3; ++i)
	    tmY[i] = tvec[i]-Y[i];
	    
	  // Update the residual vector on this thread
	  for(int i=0; i<3; ++i)
	    {
	      rVec[i] += W[i]+tmY[i];
	      rVec[i+3] += (W[next[i]]*tmY[nnext[i]]-W[nnext[i]]*tmY[next[i]]);
	      //(W[(i+1)%3]*tmY[(i+2)%3]-W[(i+2)%3]*tmY[(i+1)%3]);
	    }
	  /*std::cout<<"\n"<<p<<": "
	    <<"X: "<<X[0]<<" "<<X[1]<<" "<<X[2]
	    <<"pertPt: "<<pertPt[0]<<" "<<pertPt[1]<<" "<<pertPt[2]
	    <<"W = "<<W[0]<<" "<<W[1]<<" "<<W[2]
	    <<", tmY = "<<tmY[0]<<" "<<tmY[1]<<" "<<tmY[2]
	    <<", Y = "<<Y[0]<<" "<<Y[1]<<" "<<Y[1]<<std::flush;*/
	  //<<"<<rVec[0]<<" "<<rVec[1]<<" "<<rVec[2]
	  //	     <<" "<<rVec[3]<<" "<<rVec[4]<<" "<<rVec[5]<<std::flush;
	    
	  // skw(W)
	  Pose::HodgeStar(W, skW);
	    
	  // skw(tvec-Y);
	  Pose::HodgeStar(tmY, skT);
	    
	  // Product skT_skW
	  for(int i=0; i<3; ++i)
	    for(int j=0; j<3; ++j)
	      {
		skT_skW[i][j] = 0.;
		for(int k=0; k<3; ++k)
		  skT_skW[i][j] += skT[i][k]*skW[k][j];
	      }
		    
	  // Update the stiffness on this thread
	  // Ktt = Identity
	  // Ktr = -skw(W)
	  // Krt = skw(W)
	  // Krr = skw(tvec-Y) skw(W)
	  for(int i=0; i<3; ++i)
	    {
	      kMat[i][i] += 1.;
	      for(int j=0; j<3; ++j)
		{
		  kMat[i][j+3] -= skW[i][j];
		  kMat[i+3][j] += skW[i][j];
		  kMat[i+3][j+3] += skT_skW[i][j];
		}
	    }
	}
    }

    // Collect the contributions from each thread
    for(int i=0; i<6; ++i)
      res[i] = 0.;
    for(int i=0; i<36; ++i)
      stiff[i] = 0.;
    for(auto& ws:WrkSpc)
      for(int i=0; i<6; ++i)
	{
	  res[i] += ws.rVec[i];
	  for(int j=0; j<6; ++j)
	    stiff[6*i+j] += ws.kMat[i][j];
	}
      
    // Done
    return;
  }
    
}
