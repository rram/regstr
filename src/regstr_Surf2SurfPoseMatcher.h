// Sriramajayam

#ifndef REGSTR_SURF2SURF_POSE_MATCHER_H
#define REGSTR_SURF2SURF_POSE_MATCHER_H

#include <regstr_Mesh2SurfPoseMatcher.h>

namespace regstr
{
  //! Class to compute rigid body transformations for
  //! matching pose between a curve and an implicit surface
  class Surf2SurfPoseMatcher: public Mesh2SurfPoseMatcher
  {
  public:
    //! Constructor
    //! \param[in] pc Point cloud for which to compute
    //! \param[in] nmaxthreads max number of threads.
    //! Used for sizing memory. Defaulted to 1.
    inline Surf2SurfPoseMatcher(const std::vector<double>& coords,
				 const std::vector<int>& conn,
				 const int nmaxthreads=1)
      :Mesh2SurfPoseMatcher(coords, conn, nmaxthreads) {}
     
    // Destructor, default
    inline ~Surf2SurfPoseMatcher() {}
      
    // Copy constructor
    inline Surf2SurfPoseMatcher(const Surf2SurfPoseMatcher& obj)
      :Mesh2SurfPoseMatcher(obj) {}

    // Disable assignment operator
    Surf2SurfPoseMatcher& operator=(const Surf2SurfPoseMatcher&) = delete;

    //! Computes the implicit function values and derivatives
    virtual void GetImplicitFunction(const double* X, double& fval,
				     double* dfval=nullptr, double d2fval[][3]=nullptr) const = 0;
    
  protected:
    //! Compute integration points and weights for a given element
    void GetIntegrationPointsAndWeights(const int elm,
					std::vector<double>& Qpts, std::vector<double>& Qwts) const override;

    //! Returns the number of elements
    int GetNumElements() const override;
  };
  
}

#endif
