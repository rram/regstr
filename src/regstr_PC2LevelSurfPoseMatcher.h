// Sriramajayam

#ifndef REGSTR_PC2LEVEL_SURF_POSE_MATCHER_H
#define REGSTR_PC2LEVEL_SURF_POSE_MATCHER_H

#include <regstr_PC2SurfPoseMatcher.h>
#include <functional>

namespace regstr
{
  using ImplicitFunction = std::function<void(const double*, double& F, double*, double [][3], void*)>; //!< Function pointer to evaluate a level set function and its derivatives
	
  //! Class to compute the rigid body transformations for
  //! matching pose of a point cloud to a surface whose
  //! implicit function has been provided
  class PC2LevelSurfPoseMatcher: public PC2SurfPoseMatcher
  {
  public:
    //! Constructor
    //! \param[in] pc Point cloud to be registered
    //! \param[in] nmaxthreads Max number of threads. Defaulted to 1.
    //! \param[in] func Level set function
    //! \param[in] usrparams User defined parameters to pass to the level set function. Defaulted to null
    inline PC2LevelSurfPoseMatcher(const PointCloud& pc, ImplicitFunction func,
				   void* usrparams=nullptr, const int nmaxthreads=1)
      :PC2SurfPoseMatcher(pc, nmaxthreads), LSFunc(func), fparams(usrparams) {}

    //! Destructor
    inline ~PC2LevelSurfPoseMatcher() {}

    //! Copy constructor
    inline PC2LevelSurfPoseMatcher(const PC2LevelSurfPoseMatcher& obj)
      :PC2SurfPoseMatcher(obj), LSFunc(obj.LSFunc), fparams(obj.fparams) {}

    //! Disable assignment
    PC2LevelSurfPoseMatcher& operator=(const PC2LevelSurfPoseMatcher&) = delete;

    //! Main functionality: compute implicit function and derivatives
    inline virtual void GetImplicitFunction(const double* X, double& psi,
					    double* dpsi=nullptr,
					    double d2psi[][3]=nullptr) const override
    { LSFunc(X, psi, dpsi, d2psi, fparams); }

  private:
    ImplicitFunction LSFunc;
    void* fparams;
  };
}

#endif
