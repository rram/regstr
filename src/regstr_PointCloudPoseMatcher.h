// Sriramajayam

#ifndef REGSTR_POINT_CLOUD_POSE_MATCHER_H
#define REGSTR_POINT_CLOUD_POSE_MATCHER_H

#include <regstr_PointCloud.h>
#include <regstr_PoseMatcher.h>

namespace regstr
{
  //! Helper struct for thread-local assembly
  struct PCPoseMatcherWorkspace
  {
    double vec1[3], vec2[3], vec3[3], vec4[3]; // Intermediate vectors
    double mat1[3][3], mat2[3][3], mat3[3][3]; // Intermediate matrices
    double Err;
    double rVec[6];
    double kMat[6][6];
  };

    
  //! Class to compute isometries for matching pose between
  //! a pair of point clouds. Abstract. Correspondence calculations should be implemented
  //! by derived types.
  class PointCloudPoseMatcher: public PoseMatcher
  {
  public:
    //! Constructor
    //! \param[in] rpc Reference point cloud
    //! \param[in] ppc Point cloud for which to compute perturbation
    //! \param[in] nMaxThreads Max number of threads. Used for sizing memory. Defaulted to 1.
    PointCloudPoseMatcher(const PointCloud& rpc, const PointCloud& ppc,  const int nMaxThreads=1);
      
    //! Destructor, nothing to do
    inline virtual ~PointCloudPoseMatcher() {}
      
    //! Copy constructor
    inline PointCloudPoseMatcher(const PointCloudPoseMatcher& obj)
      :PoseMatcher(obj), refPC(obj.refPC), pertPC(obj.pertPC),
      WrkSpc(obj.WrkSpc.size()) {}

    //! Disable assignment
    PointCloudPoseMatcher& operator=(const PointCloudPoseMatcher&) = delete;

    // Main functionality:
    // Computes the error at the current state
    virtual double ComputeError() const override;
      
    // Main functionality
    // Compute the residual and its linearization
    virtual void GetLinearizations(double* res, double* stiff) const override;

    // Returns the number of threads
    int GetMaxNumThreads() const
    { return static_cast<int>(WrkSpc.size()); }

    // Main functionality: compute corresponding point pairs
    //! To be implemented by derived types
    virtual void GetCorrespondence(const int pertidx, const double* pertcoord,
				   double* refcorresp) const = 0;
      
    //! Returns access to the reference point cloud
    inline virtual const PointCloud& GetReferencePointCloud() const
    { return refPC; }
      
  private:
    // Access the thread-local workspace for assembly
    PCPoseMatcherWorkspace& GetLocalWorkspace() const;
      
    const PointCloud& refPC; //!< reference point cloud
    const PointCloud& pertPC; //!< perturbed point cloud
    mutable std::vector<PCPoseMatcherWorkspace> WrkSpc; //!< thread-safe workspaces
  };
    
}
    
#endif
