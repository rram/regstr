// Sriramajayam

#include <regstr_PointCloud.h>
#include <fstream>
#include <cassert>
#include <algorithm>
#include <list>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/index/rtree.hpp>

// Implementation of class PointCloud and OrientedPointCloud
namespace regstr
{
  // Constructor
  PointCloud::PointCloud(const std::vector<double>& pts)
    :points(pts), nPoints(pts.size()/3)
  {
    assert( (nPoints>0 && points.size()%3==0) &&
	    "regstr::PointCloud- unexpected number of points");
    ComputeBounds();
  }
  
  // Constructor
  PointCloud::PointCloud(const double* pts, const int npts)
    :points(pts, pts+3*npts), nPoints(npts)
  {
    assert( nPoints>0 && "regstr::PointCloud- unexpected number of points");
    ComputeBounds();
  }
  
  // Constructor to read data from a file
  PointCloud::PointCloud(const char* filename)
    :points({}), nPoints(0)
  {
    std::fstream pfile;
    pfile.open(filename, std::ios::in);
    pfile.precision(16);
    assert(pfile.good() && "regstr::PointCloud- could not open file");
    double val;
    pfile >> val;
    while(pfile.good())
      for(int k=0; k<3; ++k)
	{ points.push_back( val ); pfile >> val; }
    pfile.close();
    nPoints = static_cast<int>(points.size()/3);
    assert(static_cast<int>(points.size())==3*nPoints);
    ComputeBounds();
  }

  // Copy constructor
  PointCloud::PointCloud(const PointCloud& obj)
    :points(obj.points), nPoints(obj.nPoints),
     bounds{obj.bounds[0], obj.bounds[1], obj.bounds[2], obj.bounds[3], obj.bounds[4], obj.bounds[5]},
     bbcenter{obj.bbcenter[0], obj.bbcenter[1], obj.bbcenter[2]}, bbsize(obj.bbsize) {}
  

  // Returns the point set
  const std::vector<double>& PointCloud::GetPoints() const
  { return points; }

	
  // Computes the bounds for a point cloud
  void PointCloud::ComputeBounds()
  {
    // initialize bounds
    for(int k=0; k<3; ++k)
      bounds[2*k] = bounds[2*k+1] = points[k];

    // Compute bounds
    for(int p=0; p<nPoints; ++p)
      {
	const auto* X = &points[3*p];
	for(int k=0; k<3; ++k)
	  {
	    if(X[k]<bounds[2*k]) bounds[2*k] = X[k];
	    if(X[k]>bounds[2*k+1]) bounds[2*k+1] = X[k];
	  }
      }

    // Sanity check
    //for(int k=0; k<3; ++k)
    //assert(bounds[2*k+1]>bounds[2*k] && "regstr::PointCloud- unexpected bounds");
      
    // bounding box
    double delta[3];
    for(int k=0; k<3; ++k)
      {
	bbcenter[k] = 0.5*(bounds[2*k]+bounds[2*k+1]);
	delta[k] = bounds[2*k+1]-bounds[2*k];
      }
    std::sort(delta, delta+3);
    bbsize = delta[2];
    
    return;
  }

  
  // Computes the canonical transformation parameters
  // for scaling and translation
  void PointCloud::GetScalingAndTranslation(double& scaling, double* tvec) const
  {
    scaling = 1./bbsize;
    for(int k=0; k<3; ++k)
      tvec[k] = bbcenter[k];
  }
  
  // Apply scaling and translation
  // Recomputes the bounding box
  void PointCloud::ApplyScalingAndTranslation(const double& scaling, const double* tvec)
  {
    assert(scaling>0. && "regstr::PointCloud- unexpected scaling value");
    for(int p=0; p<nPoints; ++p)
      for(int k=0; k<3; ++k)
	{
	  points[3*p+k] -= tvec[k];
	  points[3*p+k] *= scaling;
	}
    ComputeBounds();
    return;
  }

  // Apply a rigid body transformation
  void PointCloud::ApplyIsometry(const double R[][3], const double* t)
  {
    double W[3];
    for(int p=0; p<nPoints; ++p)
      {
	for(int i=0; i<3; ++i)
	  {
	    W[i] = 0.;
	    for(int j=0; j<3; ++j)
	      W[i] += R[i][j]*points[3*p+j];
	  }
	for(int i=0; i<3; ++i)
	  points[3*p+i] = W[i]+t[i];
      }
      
    // Recompute the bounding box
    ComputeBounds();
    return;
  }

    
  // Computes distance for this point cloud FROM a given one
  void PointCloud::GetClosestPoints(const PointCloud& pc, const int nnb,
				    std::vector<int>& cpt) const
  {
    assert(nnb>0 && "regstr::PointCloud::GetClosestPoints- Unexpected number of neighbors found");
      
    // Create an r-tree of the reference point cloud
    typedef boost::geometry::model::point<double, 3, boost::geometry::cs::cartesian> point;
    typedef std::pair<point, int> value;
    boost::geometry::index::rtree<value, boost::geometry::index::quadratic<8>> rtree;
    point pt;
    for(int p=0; p<pc.nPoints; ++p)
      {
	pt = point(pc.points[3*p], pc.points[3*p+1], pc.points[3*p+2]);
	rtree.insert(std::make_pair(pt, p));
      }
      
    // Identify the n-th closest point
    cpt.resize(nPoints);
    std::vector<value> result;
    for(int p=0; p<nPoints; ++p)
      {
	pt = point(points[3*p], points[3*p+1], points[3*p+2]);
	result.clear();
	rtree.query(boost::geometry::index::nearest(pt, nnb), std::back_inserter(result));
	assert(static_cast<int>(result.size())==nnb);
	cpt[p] = result[nnb-1].second;
      }
    return;
  }
    
  // Computes the median of the n-closest points for this point cloud FROM a given one
  void PointCloud::ComputeMedianDistance(const PointCloud& pc, const int nnb,
					 std::vector<double>& dist) const
  {
    assert(nnb>2 && "regstr::PointCloud::ComputeDistance- Unexpected number of neighbors found");
      
    // Create an r-tree of the reference point cloud
    typedef boost::geometry::model::point<double, 3, boost::geometry::cs::cartesian> point;
    typedef std::pair<point, int> value;
    boost::geometry::index::rtree<value, boost::geometry::index::quadratic<8>> rtree;
    point pt;
    for(int p=0; p<pc.nPoints; ++p)
      {
	pt = point(pc.points[3*p], pc.points[3*p+1], pc.points[3*p+2]);
	rtree.insert(std::make_pair(pt, p));
      }
      
    // Identify the n-th closest point
    dist.resize(nPoints);
    std::vector<value> result;
    std::vector<double> nbdist(nnb);
    const int nMid = static_cast<int>(nnb/2);
    for(int p=0; p<nPoints; ++p)
      {
	pt = point(points[3*p], points[3*p+1], points[3*p+2]);
	result.clear();
	rtree.query(boost::geometry::index::nearest(pt, nnb), std::back_inserter(result));
	assert(static_cast<int>(result.size())==nnb);
	dist[p] = 0.;
	for(int i=0; i<nnb; ++i)
	  {
	    const auto& qt = result[i].first;
	    nbdist[i] = sqrt((qt.get<0>()-points[3*p+0])*(qt.get<0>()-points[3*p+0])+
			     (qt.get<1>()-points[3*p+1])*(qt.get<1>()-points[3*p+1])+
			     (qt.get<2>()-points[3*p+2])*(qt.get<2>()-points[3*p+2]));
	  }

	// Get the median distance
	std::nth_element(nbdist.begin(), nbdist.begin()+nMid, nbdist.end());
	dist[p] = nbdist[nMid];
      }
    return;
  }
      
  // Plot the point cloud in raw format
  void PointCloud::PlotXYZ(const char* filename) const
  {
    std::fstream pfile;
    pfile.open(filename, std::ios::out);
    pfile.precision(16);
    assert(pfile.good() && "regstr::PointCloud::PlotRaw- could not open file");
    for(int p=0; p<nPoints; ++p)
      pfile<<points[3*p]<<" "<<points[3*p+1]<<" "<<points[3*p+2]<<"\n";
    pfile.flush(); pfile.close();
  }
   

  // Plot the point cloud in tec format
  void PointCloud::PlotTec(const char* filename) const
  {
    // Open file to plot
    std::fstream outfile;
    outfile.open(filename, std::ios::out);
    assert(outfile.good() && "regstr::PointCloud::PlotTec- Could not open file");
    outfile.precision(16);
    
    // Line 1:
    outfile<<"VARIABLES = \"X\", \"Y\", \"Z\"";
    outfile<<"\n";
  
    // Line 2:
    outfile<<"ZONE t=\"t:0\", N="<<nPoints<<", E="<<nPoints<<", F=FEPOINT, ET=TRIANGLE";
    
    // Nodal coordinates
    for(int i=0; i<nPoints; ++i)
      outfile<<"\n"<<points[3*i]<<" "<<points[3*i+1]<<" "<<points[3*i+2];
    
    // Connectivity
    for(int i=0; i<nPoints; ++i)
      outfile<<"\n"<<i+1<<" "<<i+1<<" "<<i+1;

    outfile.flush();
    outfile.close();
    return;
  }
  

  // Identifies the set of distinct connected components
  // based on nearest neighbor searches
  int PointCloud::GetConnectedComponents(const int nnb,
					 std::vector<int>& componentNum) const
  {
    // Create an r-tree for this point cloud
    typedef boost::geometry::model::point<double, 3, boost::geometry::cs::cartesian> boost_point;
    typedef std::pair<boost_point, int> boost_value;
    boost::geometry::index::rtree<boost_value, boost::geometry::index::quadratic<8>> rtree;
    boost_point pt;
    for(int p=0; p<nPoints; ++p)
      {
	pt = boost_point(points[3*p], points[3*p+1], points[3*p+2]);
	rtree.insert(std::make_pair(pt, p));
      }
            
    // Maintain the status of a point as examined or not
    std::vector<bool> isExamined(nPoints, false);

    // Assign a component number for each point
    componentNum.resize(nPoints);
    for(int i=0; i<nPoints; ++i)
      componentNum[i] = -1;

    // Current component number
    int compNum = 0;

    // Seed point to start with.
    // If the seed is -1, the examination is complete
    int seed = 0;
      
    while(seed>=0)
      {
	// List of points to be examined
	std::list<int> points2Examine({seed});

	std::vector<boost_value> nnbresult;
	std::vector<boost_value> recip_result;
	boost_point qt;
	while(!points2Examine.empty())
	  {
	    // Pop the first point that needs to be examined
	    // Assign this the existing component number
	    int pindx = *points2Examine.begin();
	    points2Examine.pop_front();
	    componentNum[pindx] = compNum;
	    isExamined[pindx] = true;
	    pt = boost_point(points[3*pindx], points[3*pindx+1], points[3*pindx+2]);
	  
	    // Find the closest nnb neighbors
	    nnbresult.clear();
	    rtree.query(boost::geometry::index::nearest(pt, nnb), std::back_inserter(nnbresult));
	    assert(static_cast<int>(nnbresult.size())==nnb);

	    // Check reciprocity relationship between neighbors when composing clusters
	    // For a neighbor with reciprocal relationship: insert the unexamined point into the list to be examined
	    for(auto& it:nnbresult)
	      {
		int qindx = it.second;
		qt = boost_point(points[3*qindx], points[3*qindx+1], points[3*qindx+2]);
		recip_result.clear();
		rtree.query(boost::geometry::index::nearest(qt, nnb), std::back_inserter(recip_result));

		// Is pt among the nnb neigbhors of qt?
		bool flag = false;
		for(auto& jt:recip_result)
		  if(jt.second==pindx)
		    { flag = true; break; }

		// If qindx and pindx are each other's neighbors,
		// and if qindx has not been previously examined, add it to the list to be examined
		if(flag==true)
		  if(isExamined[qindx]==false)
		    {
		      points2Examine.push_back( qindx );
		      isExamined[qindx] = true;
		    }
	      }
	  }

	// Finished identifying all points in the current component.
	// Identify points in the next component
	++compNum;

	// Identify a point that has not been assigned a component yet
	seed = -1;
	for(int i=0; i<nPoints; ++i)
	  if(componentNum[i]==-1)
	    seed = i;

	// if no seed could be found, all points have been assigned a component
      }

    // Sanity check: every point should be assigned a component
    for(int i=0; i<nPoints; ++i)
      assert(componentNum[i]>=0 && "regstr::PointCloud::GetComponents- unexpcted exit");
    return compNum;
  }
    
    

  // constructor
  OrientedPointCloud::OrientedPointCloud(const std::vector<double>& pts,
					 const std::vector<double>& nrmls)
    :PointCloud(pts), normals(nrmls)
  {
    assert(static_cast<int>(normals.size())==nPoints*3 &&
	   "regstr::OrientedPointCloud- unexpected number of points");
  }
  
  // constructor
  OrientedPointCloud::OrientedPointCloud(const double* pts, const double* nrmls, const int npts)
    :PointCloud(pts, npts), normals(nrmls, nrmls+3*npts) {}

  // returns the normals
  const std::vector<double>& OrientedPointCloud::GetNormals() const
  { return normals; }
    
  // Constructor- read from a raw file
  OrientedPointCloud::OrientedPointCloud(const char* filename)
    :PointCloud(), normals({})
  {
    std::fstream pfile;
    pfile.open(filename, std::ios::in);
    pfile.precision(16);
    assert(pfile.good() && "regstr::OrientedPointCloud- could not open file");
    double val;
    pfile >> val;
    while(pfile.good())
      {
	for(int k=0; k<3; ++k)
	  { points.push_back( val ); pfile >> val; }
	for(int k=0; k<3; ++k)
	  { normals.push_back( val ); pfile >> val; }
      }
    pfile.close();
    nPoints = static_cast<int>(points.size()/3);
    assert(static_cast<int>(points.size())==3*nPoints);
    assert(static_cast<int>(normals.size())==3*nPoints);
    ComputeBounds();
  }

  // Copy constructor
  OrientedPointCloud::OrientedPointCloud(const OrientedPointCloud& obj)
    :PointCloud(obj), normals(obj.normals) {}
    
    
  // Apply a rigid body transformation
  void OrientedPointCloud::ApplyIsometry(const double R[][3], const double* t)
  {
    // Apply the transformation to the point cloud
    PointCloud::ApplyIsometry(R, t);

    // Rotate the normals
    double val;
    for(int p=0; p<nPoints; ++p)
      for(int i=0; i<3; ++i)
	{
	  val = 0.;
	  for(int j=0; j<3; ++j)
	    val += R[i][j]*normals[3*p+j];
	  normals[3*p+i] = val;
	}
    return;
  }
  
  // Plot the point cloud in raw format
  void OrientedPointCloud::PlotXYZ(const char* filename) const
  {
    std::fstream pfile;
    pfile.open(filename, std::ios::out);
    pfile.precision(16);
    assert(pfile.good() && "regstr::OrientedPointCloud::PlotXYZ- could not open file");
    for(int p=0; p<nPoints; ++p)
      pfile<<points[3*p]<<" "<<points[3*p+1]<<" "<<points[3*p+2]
	   <<" "<<normals[3*p]<<" "<<normals[3*p+1]<<" "<<normals[3*p+2]<<"\n";
    pfile.flush(); pfile.close();
  }
  
}
