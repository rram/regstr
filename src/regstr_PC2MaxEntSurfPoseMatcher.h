// Sriramajayam

#ifndef REGSTR_PC2MAX_ENT_SURF_POSE_MATCHER_H
#define REGSTR_PC2MAX_ENT_SURF_POSE_MATCHER_H

#include <regstr_PC2SurfPoseMatcher.h>
#include <mx_MaxEntSSD.h>

namespace regstr
{
  //! Helper struct for max-ent function evaluations
  struct PC2MaxEntSurfPoseMatcherWorkspace
  {
    std::vector<double> fvalues;
    std::vector<double> dfvalues;
    std::vector<double> d2fvalues;
    int nFuncs;
  };
    
  //! Class to compute the rigid body transformations for
  //! matching pose of a point cloud to a surface that is
  //! given implicitly using max-ent functions
  class PC2MaxEntSurfPoseMatcher: public PC2SurfPoseMatcher
  {
  public:
    //! Constructor
    //! \param[in] pc Point cloud to be registered
    //! \param[in] nmaxthreads Max number of threads. Defaulted to 1.
    //! \param[in] surf Max-ent surface fit
    //! \param[in] usrparams User defined parameters to pass to the level set function. Defaulted to null
    PC2MaxEntSurfPoseMatcher(const PointCloud& pc,
			     const mx::MaxEntSSD<3>& surf,
			     const int nmaxthreads=1);
    
    //! Destructor, does nothing
    inline virtual ~PC2MaxEntSurfPoseMatcher() {}
    
    //! Copy constructor
    PC2MaxEntSurfPoseMatcher(const PC2MaxEntSurfPoseMatcher&);

    //! Disable assignment
    PC2MaxEntSurfPoseMatcher& operator=(const PC2MaxEntSurfPoseMatcher&)=delete;

    //! Main functionality: compute the implicit function and derivatives
    //! Is thread safe.
    virtual void GetImplicitFunction(const double* X, double& psi,
				     double* dpsi=nullptr,
				     double d2psi[][3]=nullptr) const override;

  private:
    //! Access the local workspace for function evaluations
    PC2MaxEntSurfPoseMatcherWorkspace& GetLocalWorkspace() const;

    const mx::MaxEntSSD<3>& SurfFit; //!< Reference to the max-ent surface
    mutable std::vector<PC2MaxEntSurfPoseMatcherWorkspace> MaxEntWrkSpc;
  };
    
}

#endif
