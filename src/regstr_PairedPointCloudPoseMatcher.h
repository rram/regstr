// Sriramajayam

#ifndef REGSTR_PAIRED_POINT_CLOUD_POSE_MATCHER_H
#define REGSTR_PAIRED_POINT_CLOUD_POSE_MATCHER_H

#include <regstr_PointCloudPoseMatcher.h>

namespace regstr
{
  //! Class to compute isometries for matching pose between
  //! a pair of point clouds when the correspondence is given explcitly
  class PairedPointCloudPoseMatcher: public PointCloudPoseMatcher
  {
  public:
      
    //! Constructor
    //! \param[in] rpc Reference point cloud
    //! \param[in] ppc Point cloud for which to compute perturbation
    //! \param[in] pert2refmap Map from index in perturbed cloud to the reference
    //! cloud defining correspondence
    //! \param[in] nMaxThreads Max number of threads. Used for sizing memory. Defaulted to 1.
    inline PairedPointCloudPoseMatcher(const PointCloud& rpc, const PointCloud& ppc,
				       const std::vector<int>& pert2refmap,
				       const int nMaxThreads=1)
      :PointCloudPoseMatcher(rpc, ppc, nMaxThreads),
      RefPointCloud(rpc), Pert2RefMap(pert2refmap) {}


    //! Default destructor, nothing to do
    inline virtual ~PairedPointCloudPoseMatcher() {}

    //! Copy constructor
    inline PairedPointCloudPoseMatcher(const PairedPointCloudPoseMatcher& obj)
      :PointCloudPoseMatcher(obj), RefPointCloud(obj.RefPointCloud), Pert2RefMap(obj.Pert2RefMap) {}

    //! Disable assignment
    PairedPointCloudPoseMatcher& operator=(const PairedPointCloudPoseMatcher&) = delete;

    //! Implement correspondence
    virtual void GetCorrespondence(const int pertidx, const double* pertcoord,
				   double* refcorresp) const override
    { const int& refidx = Pert2RefMap[pertidx];
      for(int k=0; k<3; ++k)
	refcorresp[k] = RefPointCloud.points[3*refidx+k];
      return;
    }

  private:
    const PointCloud& RefPointCloud; //!< Reference point cloud
    const std::vector<int> Pert2RefMap; //!< Correspondence map for perturbed point cloud
      
  };
}

#endif
