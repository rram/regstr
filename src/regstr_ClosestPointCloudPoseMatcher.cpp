// Sriramajayam

#include <regstr_ClosestPointCloudPoseMatcher.h>

namespace regstr
{ 
  // Implementation of helper struct
  ClosestPointCorrespondenceStruct::ClosestPointCorrespondenceStruct(const PointCloud& refPC)
  {
    // Initialize the lock
    omp_init_lock(&rlock);
      
    // Populate the r-tree with points from the reference point cloud
    boost_point pt;
    for(int p=0; p<refPC.nPoints; ++p)
      {
	pt = boost_point(refPC.points[3*p], refPC.points[3*p+1], refPC.points[3*p+2]);
	rtree.insert(std::make_pair(pt, p));
      }
  }

  ClosestPointCorrespondenceStruct::~ClosestPointCorrespondenceStruct()
  { omp_destroy_lock(&rlock); }


  // Implementation of class: compute correspondence based on closest point in reference cloud
  void ClosestPointCloudPoseMatcher::GetCorrespondence(const int pertidx, const double* pertcoord,
						       double* refcorresp) const
  {
    // get access to the correspondence structure
    omp_set_lock(&CorrespStruct.rlock);
      
    // Aliases
    auto& result = CorrespStruct.result; result.clear();
    auto& pt = CorrespStruct.pt;
    auto& rtree = CorrespStruct.rtree;
      
    // Identify point in the reference cloud closest to the given one
    pt = boost_point(pertcoord[0], pertcoord[1], pertcoord[2]);
    rtree.query(boost::geometry::index::nearest(pt, 1), std::back_inserter(result));
    assert(static_cast<int>(result.size())==1);
    auto& qt = result[0].first;
    refcorresp[0] = qt.get<0>();
    refcorresp[1] = qt.get<1>();
    refcorresp[2] = qt.get<2>(); 
      
    // Release the lock
    omp_unset_lock(&CorrespStruct.rlock);
      
    return;
  }
}
 
  
