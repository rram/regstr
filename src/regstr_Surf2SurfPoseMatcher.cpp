// Sriramajayam

#include <regstr_Surf2SurfPoseMatcher.h>
#include <cmath>

namespace regstr
{
  // Compute integration points and weights for a given element
  void Surf2SurfPoseMatcher::GetIntegrationPointsAndWeights(const int elm,
							     std::vector<double>& Qpts,
							     std::vector<double>& Qwts) const
  {
    const int SPD = 3;
    const int nQuad = 3; // 3 point rule
    if(static_cast<int>(Qpts.size())<SPD*nQuad) Qpts.resize(SPD*nQuad);
    if(static_cast<int>(Qwts.size())<nQuad) Qwts.resize(nQuad);
    const int a = connectivity[3*elm+0]-1;
    const int b = connectivity[3*elm+1]-1;
    const int c = connectivity[3*elm+2]-1;
    const double* A = &xyzcoords[SPD*a];
    const double* B = &xyzcoords[SPD*b];
    const double* C = &xyzcoords[SPD*c];

    // Area of this element
    const double lenAB = std::sqrt((B[0]-A[0])*(B[0]-A[0]) + (B[1]-A[1])*(B[1]-A[1]) + (B[2]-A[2])*(B[2]-A[2]));
    const double lenAC = std::sqrt((C[0]-A[0])*(C[0]-A[0]) + (C[1]-A[1])*(C[1]-A[1]) + (C[2]-A[2])*(C[2]-A[2]));
    const double lenBC = std::sqrt((C[0]-B[0])*(C[0]-B[0]) + (C[1]-B[1])*(C[1]-B[1]) + (C[2]-B[2])*(C[2]-B[2]));
    double s = 0.5*(lenAB+lenAC+lenBC);
    double Area = std::sqrt(s*(s-lenAB)*(s-lenAC)*(s-lenBC));

    // Integration points
    const double lambda[] = {2./3.,1./6.,1./6.,
			     1./6.,2./3.,1./6.,
			     1./6.,1./6.,2./3.};
    for(int q=0; q<nQuad; ++q)
      {
	for(int k=0; k<SPD; ++k)
	  Qpts[SPD*q+k] = lambda[3*q+0]*A[k] + lambda[3*q+1]*B[k] + lambda[3*q+2]*C[k];
	Qwts[q] = Area/3.;
      }
    return;
  }

  // Returns the number of elements
 int Surf2SurfPoseMatcher::GetNumElements() const
  { return static_cast<int>(connectivity.size()/3); }
}
