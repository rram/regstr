// Sriramajayam

#ifndef REGSTR_MESH2SURF_POSE_MATCHER_H
#define REGSTR_MESH2SURF_POSE_MATCHER_H

#include <regstr_PoseMatcher.h>
#include <regstr_PC2SurfPoseMatcher.h>

namespace regstr
{
  // Helper struct for implicit function evaluations
  using Mesh2SurfPoseMatcherWorkspace = PC2SurfPoseMatcherWorkspace;
    
  //! Class to compute rigid body transformations for
  //! matching pose between a curve and an implicit surface
  class Mesh2SurfPoseMatcher: public PoseMatcher
  {
  public:
    //! Constructor
    //! \param[in] pc Point cloud for which to compute
    //! \param[in] nmaxthreads max number of threads.
    //! Used for sizing memory. Defaulted to 1.
    Mesh2SurfPoseMatcher(const std::vector<double>& coords,
			 const std::vector<int>& conn,
			 const int nmaxthreads=1);
     
    // Destructor, default
    inline ~Mesh2SurfPoseMatcher() {}
      
    // Copy constructor
    Mesh2SurfPoseMatcher(const Mesh2SurfPoseMatcher&);

    // Disable assignment operator
    Mesh2SurfPoseMatcher& operator=(const Mesh2SurfPoseMatcher&) = delete;

    // Main functionality:
    // Computes the error at the current state
    virtual double ComputeError() const override;
      
    // Main functionality
    // Compute the residual and its linearization
    virtual void GetLinearizations(double* res, double* stiff) const override;

    //! Returns the number of threads
    inline int GetMaxNumThreads() const
    { return static_cast<int>(WrkSpc.size()); }

    //! Computes the implicit function values and derivatives
    virtual void GetImplicitFunction(const double* X, double& fval,
				     double* dfval=nullptr, double d2fval[][3]=nullptr) const = 0;

  protected:
    //! Compute integration points and weights for a given element
    virtual void GetIntegrationPointsAndWeights(const int elm,
						std::vector<double>& Qpts, std::vector<double>& Qwts) const = 0;

    //! Returns the number of elements
    virtual int GetNumElements() const = 0;

    const std::vector<double>& xyzcoords;
    const std::vector<int>& connectivity;
    
  private:
    // Access the local workspace for residual/stiffness
    // calculations
    Mesh2SurfPoseMatcherWorkspace& GetLocalWorkspace() const;
    mutable std::vector<Mesh2SurfPoseMatcherWorkspace> WrkSpc; 
  };
  
}

#endif
