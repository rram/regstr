// Sriramajayam

#include <regstr_Surf2MaxEntSurfPoseMatcher.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace regstr
{
  // Constructor
  Surf2MaxEntSurfPoseMatcher::
  Surf2MaxEntSurfPoseMatcher(const std::vector<double>& coords,
			     const std::vector<int>& conn, 
			     const mx::MaxEntSSD<3>& surf,
			     const int nmaxthreads)
    :Surf2SurfPoseMatcher(coords, conn, nmaxthreads), SurfFit(surf)
  {
    // Allocate memory for thread-local workspaces
    assert(nmaxthreads>=1 &&
	   "regstr::Surf2MaxEntSurfPoseMatcher- Unexpected max-number of threads");
#ifndef _OPENMP
    assert(nmaxthreads==1 &&
	   "regstr::Surf2MaxEntSurfPoseMatcher- Use 1 thread without openmp");
#endif

    // Check that the number of threads is consistent
    //assert(nmaxthreads==surf.GetNumThreads() &&
    //	   "regstr::Surf2MaxEntSurfPoseMatcher- Use same number of threads as MaxEntSurfaceFit object");
      
    MaxEntWrkSpc.resize(nmaxthreads);
    const int nDof = SurfFit.GetNumDofs();
    for(int thr=0; thr<nmaxthreads; ++thr)
      {
	MaxEntWrkSpc[thr].fvalues.resize(nDof);
	MaxEntWrkSpc[thr].dfvalues.resize(3*nDof);
	MaxEntWrkSpc[thr].d2fvalues.resize(9*nDof);
      }
    return;
  }


  // Copy constructor
  Surf2MaxEntSurfPoseMatcher::Surf2MaxEntSurfPoseMatcher(const Surf2MaxEntSurfPoseMatcher& obj)
    :Surf2SurfPoseMatcher(obj), SurfFit(obj.SurfFit)
  {
    // Allocate local workspace
    const int nMaxThreads = static_cast<int>(obj.MaxEntWrkSpc.size());
    MaxEntWrkSpc.resize(nMaxThreads);
    for(int thr=0; thr<nMaxThreads; ++thr)
      {
	MaxEntWrkSpc[thr].fvalues.resize(obj.MaxEntWrkSpc[thr].fvalues.size());
	MaxEntWrkSpc[thr].dfvalues.resize(obj.MaxEntWrkSpc[thr].dfvalues.size());
	MaxEntWrkSpc[thr].d2fvalues.resize(obj.MaxEntWrkSpc[thr].d2fvalues.size());
      }
  }



  // Access the local workspace
  Surf2MaxEntSurfPoseMatcherWorkspace&
  Surf2MaxEntSurfPoseMatcher::GetLocalWorkspace() const
  {
    int thrnum = 0;
#ifdef _OPENMP
    thrnum = omp_get_thread_num();
    assert(thrnum<static_cast<int>(MaxEntWrkSpc.size()) &&
	   "regstr::Surf2MaxEntSurfPoseMatcher::GetLocalWorkspace- exceeded max. thread number");
#endif
    return MaxEntWrkSpc[thrnum];
  }
    

  // Main functionality: compute the implicit function and derivatives
  void Surf2MaxEntSurfPoseMatcher::GetImplicitFunction(const double* X, double& psi,
						     double* dpsi, double d2psi[][3]) const
  {
    // Access the max-ent functions and dof values
    const auto& ME = SurfFit.GetMaxEntFunctions();
    const auto& dofs = SurfFit.GetDofs();

    // Initialize function values and derivatives
    psi = 0.;
    if(dpsi!=nullptr)
      for(int i=0; i<3; ++i) dpsi[i] = 0.;
    if(d2psi!=nullptr)
      for(int i=0; i<3; ++i)
	for(int j=0; j<3; ++j)
	  d2psi[i][j] = 0.;
      
    // Access thread-local variables for function evaluations
    auto& ws = GetLocalWorkspace();
    auto& nFuncs = ws.nFuncs;
    auto* fvalues = &ws.fvalues[0];
    auto* dfvalues = (dpsi==nullptr) ? nullptr : &ws.dfvalues[0];
    auto* d2fvalues = (d2psi==nullptr) ? nullptr : &ws.d2fvalues[0];
      
    // Evaluate shape functions, derivatives and hessians
    const auto& suppvec = ME.Evaluate(X, fvalues, dfvalues, d2fvalues);
    nFuncs = static_cast<int>(suppvec.size());
	
    // Compute the implicit function, derivative and hessian
    for(int i=0; i<nFuncs; ++i)
      {
	const auto& dofval = dofs[suppvec[i]];
	psi += dofval*fvalues[i];
	if(dpsi!=nullptr)
	  for(int j=0; j<3; ++j)
	    dpsi[j] += dofval*dfvalues[3*i+j];
	if(d2psi!=nullptr)
	  for(int j=0; j<3; ++j)
	    for(int k=0; k<3; ++k)
	      d2psi[j][k] += dofval*d2fvalues[9*i+3*j+k];
      }
    return;
  }
}
