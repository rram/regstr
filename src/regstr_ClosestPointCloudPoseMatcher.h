// Sriramajayam

#ifndef REGSTR_CLOSEST_POINT_CLOUD_POSE_MATCHER_H
#define REGSTR_CLOSEST_POINT_CLOUD_POSE_MATCHER_H

#include <regstr_PointCloudPoseMatcher.h>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/index/rtree.hpp>
#ifdef _OPENMP
#include <omp.h>
#endif

namespace regstr
{
  typedef boost::geometry::model::point<double, 3, boost::geometry::cs::cartesian> boost_point;
  typedef std::pair<boost_point, int> boost_value;

  //! Helper struct for correspondence searches
  struct ClosestPointCorrespondenceStruct
  {
    ClosestPointCorrespondenceStruct(const PointCloud& pc);
    ~ClosestPointCorrespondenceStruct();
    ClosestPointCorrespondenceStruct(const ClosestPointCorrespondenceStruct&) = delete;
    omp_lock_t rlock; // Lock for access the rtree
    boost::geometry::index::rtree<boost_value, boost::geometry::index::quadratic<8>> rtree;
    std::vector<boost_value> result; // Intermediate variable
    boost_point pt; // Intermediate variable
  };

  //! Class to compute isometries for matching pose between
  //! a pair of point clouds using the closest point correspondence
  class ClosestPointCloudPoseMatcher: public PointCloudPoseMatcher
  {
  public:
    //! Constructor
    //! \param[in] rpc Reference point cloud
    //! \param[in] ppc Point cloud for which to compute perturbation
    //! \param[in] nMaxThreads Max number of threads. Used for sizing memory. Defaulted to 1.
    inline ClosestPointCloudPoseMatcher(const PointCloud& rpc, const PointCloud& ppc,
					const int nMaxThreads=1)
      :PointCloudPoseMatcher(rpc, ppc, nMaxThreads), CorrespStruct(rpc) {}

    //! Copy constructor
    inline ClosestPointCloudPoseMatcher(const ClosestPointCloudPoseMatcher& obj)
      :PointCloudPoseMatcher(obj), CorrespStruct(obj.GetReferencePointCloud()) {}

    //! Destructor
    inline virtual ~ClosestPointCloudPoseMatcher() {}

    //! Disable assignment
    ClosestPointCloudPoseMatcher& operator=(const ClosestPointCloudPoseMatcher&) = delete;

    // Main functionality: compute corresponding point pairs
    void GetCorrespondence(const int pertidx, const double* pertcoord,
			   double* refcorresp) const override;

  private:
    mutable ClosestPointCorrespondenceStruct CorrespStruct; //!< Compute closest point in reference cloud using a range tree
  };
}
    
    
#endif
