// Sriramajayam

#include <regstr_PC2LevelSurfPoseMatcher.h>
#include <random>
#include <omp.h>
#include <iostream>
#include <cassert>

using namespace regstr;

// Generate a random rigid body transformation
void GetRandomIsometry(double Rot[][3], double* tvec);

// Consistency test for linearizations
void ConsistencyTest(PC2LevelSurfPoseMatcher& matcher);

// Level set function
void LevelFunction(const double* X, double& psi,
		   double* dpsi, double d2psi[][3], void* params);

// Consistency test for linearizations
void ConsistencyTest(PC2LevelSurfPoseMatcher& matcher);

int main()
{
  omp_set_num_threads(1);

  // Create the point cloud
  std::vector<double> pts;
  for(int i=0; i<20; ++i)
    for(int j=0; j<20; ++j)
      {
	const double x = 0.1*static_cast<double>(i);
	const double y = 0.1*static_cast<double>(j);
	const double z = y*std::sin(x)-x*std::cos(y);
	pts.push_back(x);
	pts.push_back(y);
	pts.push_back(z);
      }
  PointCloud PC(pts);
  PC.PlotXYZ((char*)"pc.xyz");

  // Get a random isometry
  double R0[3][3], t0[3];
  GetRandomIsometry(R0, t0);

  // Perturb the point cloud
  PC.ApplyIsometry(R0, t0);
  PC.PlotXYZ((char*)"pert.xyz");
  
  // Level set function
  ImplicitFunction LSFunc = LevelFunction;

  // Pose matcher
  PC2LevelSurfPoseMatcher matcher(PC, LSFunc);

  std::cout<<"\nInitial error: "<<matcher.ComputeError()<<std::flush;
  
  // Iterate
  double solNorm, resNorm, Err;
  for(int iter=0; iter<50; ++iter)
    {
      // Do a consistency test
      ConsistencyTest(matcher);
      
      std::cout<<"\nIter "<<iter+1<<": "<<std::flush;
      matcher.Iterate(1., solNorm, resNorm);
      Err = matcher.ComputeError();
      std::cout<<"solNorm: "<<solNorm<<", resNorm: "<<resNorm
	       <<", Err: "<<Err<<std::flush;
      if(resNorm<1.e-8) break;
    }
  double Rot[3][3], tvec[3];
  matcher.GetPose(Rot, tvec);
  PC.ApplyIsometry(Rot, tvec);
  PC.PlotXYZ((char*)"reg.xyz");
}
	

// Level set function
void LevelFunction(const double* X, double& psi,
		   double* dpsi, double d2psi[][3], void* params)
{
  const double& x = X[0];
  const double& y = X[1];
  const double& z = X[2];

  // Function value
  psi = y*std::sin(x)-x*std::cos(y)-z;

  // Derivative
  if(dpsi!=nullptr)
    {
      dpsi[0] = y*std::cos(x)-std::cos(y);
      dpsi[1] = std::sin(x)+x*std::sin(y);
      dpsi[2] = -1.;
    }

  if(d2psi!=nullptr)
    {
      d2psi[0][0] = -y*std::sin(x);          d2psi[0][1] = std::cos(x)+std::sin(y); d2psi[0][2] = 0.;
      d2psi[1][0] = std::cos(x)+std::sin(y); d2psi[1][1] = x*std::cos(y);           d2psi[1][2] = 0.;
      d2psi[2][0] = 0.; d2psi[2][1] = 0.; d2psi[2][2] = 0.;
    }
  return;
}

  
// Generate a random rigid body transformation
void GetRandomIsometry(double Rot[][3], double* tvec)
{
  std::random_device rd;  
  std::mt19937 gen(rd()); 
  std::uniform_real_distribution<> dis(-0.1, 0.1);
  double theta[3];
  for(int i=0; i<3; ++i)
    { tvec[i] = dis(gen); theta[i] = dis(gen); }
  double skw[3][3];
  skw[0][0] = 0.;        skw[0][1] = -theta[2]; skw[0][2] = theta[1];
  skw[1][0] = theta[2];  skw[1][1] = 0.;        skw[1][2] = -theta[0];
  skw[2][0] = -theta[1]; skw[2][1] = theta[0];  skw[2][2] = 0.;
  Pose::ExpSO3(skw, Rot);

  return;
}



// Consistency test for linearizations
void ConsistencyTest(PC2LevelSurfPoseMatcher& matcher)
{
  const double EPS = 1.e-3;
  const double TOL = 1.e-1;
  double tvec[3], tplus[3], tminus[3];
  double R[3][3], Rplus[3][3], Rminus[3][3];

  // Get the current state
  matcher.GetPose(R, tvec);
  
  // Compute the linearizations at this state
  double kMat[6*6], rVec[6];
  double rPlus[6], rMinus[6];
  double kPlus[6*6], kMinus[6*6];
  double rnum, knum;
  matcher.GetLinearizations(rVec, kMat);
  
  // Consistency of residuals wrt variations in translation
  for(int i=0; i<3; ++i)
    {
      for(int j=0; j<3; ++j)
	{ tplus[j] = tvec[j]; tminus[j] = tvec[j]; }
      tplus[i] += EPS;
      tminus[i] -= EPS;

      // Positive perturbation
      matcher.SetPose(R, tplus);
      double Eplus = matcher.ComputeError();
      matcher.GetLinearizations(rPlus, kPlus);
      
      // Negative perturbation
      matcher.SetPose(R, tminus);
      double Eminus = matcher.ComputeError();
      matcher.GetLinearizations(rMinus, kMinus);

      // reset the state
      matcher.SetPose(R, tvec);
      
      // Check values
      rnum = (Eplus-Eminus)/(2.*EPS);
      assert(std::abs(rnum-rVec[i])<TOL);
      for(int j=0; j<6; ++j)
	{
	  knum = (rPlus[j]-rMinus[j])/(2.*EPS);
	  assert(std::abs(knum-kMat[6*j+i])<TOL);
	}
    }

  // Consistency of residuals wrt variations in angles
  double aplus[3], aminus[3];
  double skPlus[3][3], skMinus[3][3];
  double expPlus[3][3], expMinus[3][3];
  for(int i=0; i<3; ++i)
    {
      // Positive and negative perturbations
      for(int j=0; j<3; ++j)
	{ aplus[j] = 0.; aminus[j] = 0.; }
      aplus[i] = EPS;
      Pose::HodgeStar(aplus, skPlus);
      Pose::ExpSO3(skPlus, expPlus);
      aminus[i] = -EPS;
      Pose::HodgeStar(aminus, skMinus);
      Pose::ExpSO3(skMinus, expMinus);
      for(int j=0; j<3; ++j)
	for(int k=0; k<3; ++k)
	  {
	    Rplus[j][k] = Rminus[j][k] = 0.;
	    for(int L=0; L<3; ++L)
	      {
		Rplus[j][k] += expPlus[j][L]*R[L][k];
		Rminus[j][k] += expMinus[j][L]*R[L][k];
	      }
	  }

      // Err at +ve perturbation
      matcher.SetPose(Rplus, tvec);
      double Eplus = matcher.ComputeError();
      matcher.GetLinearizations(rPlus, kPlus);

      // Err at -ve perturbation
      matcher.SetPose(Rminus, tvec);
      double Eminus = matcher.ComputeError();
      matcher.GetLinearizations(rMinus, kMinus);

      // Reset the base state
      matcher.SetPose(R, tvec);

      // Compare values
      rnum = (Eplus-Eminus)/(2.*EPS);
      assert(std::abs(rnum-rVec[3+i])<TOL);
      for(int j=0; j<6; ++j)
	{
	  knum = (rPlus[j]-rMinus[j])/(2.*EPS);
	  assert(std::abs(knum-kMat[6*j+(i+3)])<TOL);
	}
    }
}
