// Sriramajayam

#include <regstr_ClosestPointCloudPoseMatcher.h>
#include <omp.h>
#include <random>

using namespace regstr;

// Generate a random rigid body transformation
void GetRandomIsometry(double Rot[][3], double* tvec);

int main()
{
  const int nThreads = 2;
  omp_set_num_threads(nThreads);
  
  // Read the point cloud defining the surface
  OrientedPointCloud refPC((char*)"data/pc.pts");
  refPC.PlotXYZ((char*)"pc.xyz");
  
  
  // Fabricate point cloud by slight transformation of refPC
  PointCloud PC(refPC);
  double tvec[3], Rot[3][3];
  GetRandomIsometry(Rot, tvec);
  PC.ApplyIsometry(Rot, tvec);
  PC.PlotXYZ((char*)"pert.xyz");
  
  // Create pose matcher
  ClosestPointCloudPoseMatcher matcher(refPC, PC, nThreads);
  
  // Iterate
  double solNorm, resNorm;
  for(int iter=0; iter<10; ++iter)
    {
      matcher.Iterate(1., solNorm, resNorm);
      std::cout<<"\nsol norm: "<<solNorm<<", res norm: "<<resNorm
	       <<", error: "<<matcher.ComputeError()<<std::flush;
    }
  
  // Plot this solution
  double solR[3][3], solt[3];
  matcher.GetPose(solR, solt);
  PointCloud solPC(PC);
  solPC.ApplyIsometry(solR, solt);
  solPC.PlotXYZ((char*)"sol.xyz");
  
}


// Generate a random rigid body transformation
void GetRandomIsometry(double Rot[][3], double* tvec)
{
  std::random_device rd;  
  std::mt19937 gen(rd()); 
  std::uniform_real_distribution<> dis(-0.05, 0.05);
  double theta[3];
  for(int i=0; i<3; ++i)
    { tvec[i] = dis(gen); theta[i] = dis(gen); }
  double skw[3][3];
  skw[0][0] = 0.;        skw[0][1] = -theta[2]; skw[0][2] = theta[1];
  skw[1][0] = theta[2];  skw[1][1] = 0.;        skw[1][2] = -theta[0];
  skw[2][0] = -theta[1]; skw[2][1] = theta[0];  skw[2][2] = 0.;
  Pose::ExpSO3(skw, Rot);

  return;
}
