// Sriramajayam

#include <regstr_PairedPointCloudPoseMatcher.h>
#include <omp.h>
#include <random>
#include <iostream>
#include <cassert>

using namespace regstr;

// Generate a random rigid body transformation
void GetRandomIsometry(double Rot[][3]);

// Consistency test for linearizations
void ConsistencyTest(PointCloudPoseMatcher& matcher);

int main()
{
  const int nThreads = 1;
  omp_set_num_threads(nThreads);
  
  // Read the point cloud defining the surface
  OrientedPointCloud refPC((char*)"data/pc.pts");
  refPC.PlotXYZ((char*)"pc.xyz");
  
  
  // Fabricate point cloud by slight transformation of refPC
  PointCloud PC(refPC);
  double Rot[3][3];
  GetRandomIsometry(Rot);
  double tvec[] = {0.,0.,0.};
  PC.ApplyIsometry(Rot, tvec);
  PC.PlotXYZ((char*)"pert.xyz");

  // Explicit correspondence
  std::vector<int> correspondence(refPC.nPoints);
  for(int i=0; i<refPC.nPoints; ++i)
    correspondence[i] = i;
    
  // Create pose matcher
  PairedPointCloudPoseMatcher matcher(refPC, PC, correspondence, nThreads);
  
  // Iterate 
  double solNorm, resNorm;
  for(int iter=0; iter<10; ++iter)
    {
      // Consistency at this configuration
      ConsistencyTest(matcher);

      // Solve and update
      matcher.Iterate(1., solNorm, resNorm);
      std::cout<<"\nsol norm: "<<solNorm<<", res norm: "<<resNorm
	       <<", error: "<<matcher.ComputeError()<<std::flush;

      if(resNorm<1.e-9) break;
    }
  
  // Plot this solution
  double solR[3][3], solt[3];
  matcher.GetPose(solR, solt);
  PointCloud solPC(PC);
  solPC.ApplyIsometry(solR, solt);
  solPC.PlotXYZ((char*)"sol.xyz");
}


// Generate a random rigid body transformation
void GetRandomIsometry(double Rot[][3])
{
  std::random_device rd;  
  std::mt19937 gen(rd()); 
  std::uniform_real_distribution<> dis(-1., 1.);
  double theta[3];
  for(int i=0; i<3; ++i) theta[i] = dis(gen);
  double skw[3][3];
  skw[0][0] = 0.;        skw[0][1] = -theta[2]; skw[0][2] = theta[1];
  skw[1][0] = theta[2];  skw[1][1] = 0.;        skw[1][2] = -theta[0];
  skw[2][0] = -theta[1]; skw[2][1] = theta[0];  skw[2][2] = 0.;
  Pose::ExpSO3(skw, Rot);

  return;
}


// Consistency test for linearizations
void ConsistencyTest(PointCloudPoseMatcher& matcher)
{
  const double EPS = 1.e-4;
  const double TOL = 1.e-1;
  double tvec[3], tplus[3], tminus[3];
  double R[3][3], Rplus[3][3], Rminus[3][3];

  // Get the current state
  matcher.GetPose(R, tvec);
  
  // Compute the linearizations at this state
  double kMat[6*6], rVec[6];
  double rPlus[6], rMinus[6];
  double kPlus[6*6], kMinus[6*6];
  double rnum, knum;
  matcher.GetLinearizations(rVec, kMat);
  
  // Consistency of residuals wrt variations in translation
  for(int i=0; i<3; ++i)
    {
      for(int j=0; j<3; ++j)
	{ tplus[j] = tvec[j]; tminus[j] = tvec[j]; }
      tplus[i] += EPS;
      tminus[i] -= EPS;

      // Positive perturbation
      matcher.SetPose(R, tplus);
      double Eplus = matcher.ComputeError();
      matcher.GetLinearizations(rPlus, kPlus);
      
      // Negative perturbation
      matcher.SetPose(R, tminus);
      double Eminus = matcher.ComputeError();
      matcher.GetLinearizations(rMinus, kMinus);

      // reset the state
      matcher.SetPose(R, tvec);
      
      // Check values
      rnum = (Eplus-Eminus)/(2.*EPS);
      //std::cout<<"\n"<<rVec[i]<<" should be "<<rnum; std::fflush( stdout );
      assert(std::abs(rnum-rVec[i])<TOL);
      for(int j=0; j<6; ++j)
	{
	  knum = (rPlus[j]-rMinus[j])/(2.*EPS);
	  //std::cout<<"\n"<<kMat[6*j+i]<<" should be "<<knum; std::fflush( stdout );
	  assert(std::abs(knum-kMat[6*j+i])<TOL);
	}
    }
  
  // Consistency of residuals wrt variations in angles
  double aplus[3], aminus[3];
  double skPlus[3][3], skMinus[3][3];
  double expPlus[3][3], expMinus[3][3];
  for(int i=0; i<3; ++i)
    {
      // Positive and negative perturbations
      for(int j=0; j<3; ++j)
	{ aplus[j] = 0.; aminus[j] = 0.; }
      aplus[i] = EPS;
      Pose::HodgeStar(aplus, skPlus);
      Pose::ExpSO3(skPlus, expPlus);
      aminus[i] = -EPS;
      Pose::HodgeStar(aminus, skMinus);
      Pose::ExpSO3(skMinus, expMinus);
      for(int j=0; j<3; ++j)
	for(int k=0; k<3; ++k)
	  {
	    Rplus[j][k] = Rminus[j][k] = 0.;
	    for(int L=0; L<3; ++L)
	      {
		Rplus[j][k] += expPlus[j][L]*R[L][k];
		Rminus[j][k] += expMinus[j][L]*R[L][k];
	      }
	  }

      // Err at +ve perturbation
      matcher.SetPose(Rplus, tvec);
      double Eplus = matcher.ComputeError();
      matcher.GetLinearizations(rPlus, kPlus);

      // Err at -ve perturbation
      matcher.SetPose(Rminus, tvec);
      double Eminus = matcher.ComputeError();
      matcher.GetLinearizations(rMinus, kMinus);

      // Reset the base state
      matcher.SetPose(R, tvec);

      // Compare values
      rnum = (Eplus-Eminus)/(2.*EPS);
      //std::cout<<"\n"<<rVec[i]<<" should be "<<rnum; std::fflush( stdout );
      assert(std::abs(rnum-rVec[3+i])<TOL);
      for(int j=0; j<6; ++j)
	{
	  knum = (rPlus[j]-rMinus[j])/(2.*EPS);
	  //std::cout<<"\n"<<kMat[6*j+(i+3)]<<" should be "<<knum; std::fflush( stdout );
	  assert(std::abs(knum-kMat[6*j+(i+3)])<TOL);
	}
    }
}
