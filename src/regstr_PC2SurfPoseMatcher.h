// Sriramajayam

#ifndef REGSTR_PC2SURF_POSE_MATCHER_H
#define REGSTR_PC2SURF_POSE_MATCHER_H

#include <regstr_PointCloud.h>
#include <regstr_PoseMatcher.h>

namespace regstr
{
  // Helper struct for implicit function evaluations
  struct PC2SurfPoseMatcherWorkspace
  {
    double psi, dpsi[3], d2psi[3][3];
    double dofval;
    double v1[3], v2[3], v3[3]; // interm vectors
    double M1[3][3], M2[3][3],  M3[3][3], M4[3][3], M5[3][3]; // interm matrices
    double Err;
    double rVec[6];
    double kMat[6][6];
  };

  //! Class to compute rigid body transformations for
  //! matching pose between a point cloud and an implicit surface
  //! Class is abstract because the implicit function is to be specified by
  //! the derived classes
  class PC2SurfPoseMatcher: public PoseMatcher
  {
  public:
    //! Constructor
    //! \param[in] pc Point cloud for which to compute
    //! \param[in] nmaxthreads max number of threads.
    //! Used for sizing memory. Defaulted to 1.
    PC2SurfPoseMatcher(const PointCloud& pc, const int nmaxthreads=1);
     
    // Destructor, default
    inline ~PC2SurfPoseMatcher() {}
      
    // Copy constructor
    PC2SurfPoseMatcher(const PC2SurfPoseMatcher&);

    // Disable assignment operator
    PC2SurfPoseMatcher& operator=(const PC2SurfPoseMatcher&) = delete;

    // Main functionality:
    // Computes the error at the current state
    virtual double ComputeError() const override;
      
    // Main functionality
    // Compute the residual and its linearization
    virtual void GetLinearizations(double* res, double* stiff) const override;

    //! Returns the number of threads
    inline int GetMaxNumThreads() const
    { return static_cast<int>(WrkSpc.size()); }

    //! Computes the implicit function values and derivatives
    virtual void GetImplicitFunction(const double* X, double& fval,
				     double* dfval=nullptr, double d2fval[][3]=nullptr) const = 0;
      
  private:
    // Access the local workspace for residual/stiffness
    // calculations
    PC2SurfPoseMatcherWorkspace& GetLocalWorkspace() const;

    const PointCloud& PC;
    mutable std::vector<PC2SurfPoseMatcherWorkspace> WrkSpc; 
  };
}

#endif
