// Sriramajayam

#ifndef REGSTR_POINT_CLOUD_H
#define REGSTR_POINT_CLOUD_H

#include <vector>

namespace regstr
{
  // Struct to encapsulate data about a point cloud
  class PointCloud
  {
  public:
    // Constructor
    PointCloud(const std::vector<double>& pts);

    // Constructor
    PointCloud(const double* pts, const int npts);

    // Constructor to read data from a file
    PointCloud(const char* filename);

    // Copy constructor
    PointCloud(const PointCloud&);

    // Returns the point set
    const std::vector<double>& GetPoints() const;

    // Computes the canonical transformation parameters
    // for scaling and translation
    void GetScalingAndTranslation(double& scaling, double* tvec) const;

    // Apply scaling and translation
    // Recomputes the bounding box
    void ApplyScalingAndTranslation(const double& scaling, const double* tvec);

    // Apply a rigid body transformation
    virtual void ApplyIsometry(const double R[][3], const double* t);

    // Computes the nth-closest point for this point cloud FROM a given one
    void GetClosestPoints(const PointCloud& pc, const int nnb,
			  std::vector<int>& cpt) const;

    // Computes the median of the distance to the
    // n-closest points for this point cloud FROM a given one
    void ComputeMedianDistance(const PointCloud& pc, const int nnb,
			       std::vector<double>& dist) const;
      
    // Plot the point cloud in raw format
    virtual void PlotXYZ(const char* filename) const;
    
    // Plot the point cloud in tec format
    void PlotTec(const char* filename) const;

    // Identifies the set of distinct connected components
    // based on nearest neighbor searches
    int GetConnectedComponents(const int nnb, std::vector<int>& componentNum) const;
      
    // Members
    std::vector<double> points; // Point coordinates
    int nPoints; // Number of points
    double bounds[6]; // Bounds for coordinates
    double bbcenter[3]; // coordinates of center of bounding box
    double bbsize; // Size of bounding box

  protected:
    // For construction by derived types
    inline PointCloud(): points({}), nPoints(0) {}
      
    // Computes the bounds for a point cloud
    void ComputeBounds();
  };


  // Encapsulate data and methods for a point cloud with normals
  class OrientedPointCloud: public PointCloud
  {
  public:

    // constructor
    OrientedPointCloud(const std::vector<double>& pts,
		       const std::vector<double>& nrmls);

    // constructor
    OrientedPointCloud(const double* pts, const double* nrmls, const int npts);
    
    // Read from a raw file
    OrientedPointCloud(const char* filename);

    // Copy constructor
    OrientedPointCloud(const OrientedPointCloud&);

    // Returns the normals
    const std::vector<double>& GetNormals() const;
      
    // Apply a rigid body transformation
    void ApplyIsometry(const double R[][3], const double* t) override;
    
    // Plot the point cloud in XYZ format
    void PlotXYZ(const char* filename) const override;

    // Members
    std::vector<double> normals; // normals
  };
}

#endif
