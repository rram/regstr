// Sriramajayam

#include <regstr_Mesh2SurfPoseMatcher.h>
#include <cassert>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace regstr
{
  // Constructor
  Mesh2SurfPoseMatcher::Mesh2SurfPoseMatcher(const std::vector<double>& coords,
					     const std::vector<int>& conn,
					     const int nMaxThreads)
    :PoseMatcher(),
     xyzcoords(coords),
     connectivity(conn)
  {
    // Allocate memory for thread-local workspaces
    assert(nMaxThreads>=1 && "regstr::Mesh2SurfPoseMatcher- Unexpected max-number of threads");
#ifndef _OPENMP
    assert(nMaxThreads==1 && "regstr::Mesh2SurfPoseMatcher- Use 1 thread without openmp");
#endif

    WrkSpc.resize(nMaxThreads);
    return;
  }

  
  // Copy constructor
  Mesh2SurfPoseMatcher::Mesh2SurfPoseMatcher(const Mesh2SurfPoseMatcher& obj)
    :PoseMatcher(obj),
     xyzcoords(obj.xyzcoords),
     connectivity(obj.connectivity),
     WrkSpc(obj.WrkSpc.size()) {}

  // Access the local workspace
  Mesh2SurfPoseMatcherWorkspace& Mesh2SurfPoseMatcher::GetLocalWorkspace() const
  {
    int thrnum = 0;
#ifdef _OPENMP
    thrnum = omp_get_thread_num();
    assert(thrnum<static_cast<int>(WrkSpc.size()) &&
	   "regstr::Mesh2SurfPoseMatcher::GetLocalWorkspace- exceeded max. thread number");
#endif
    return WrkSpc[thrnum];
  }

  // Computes the error at the current state
  double Mesh2SurfPoseMatcher::ComputeError() const
  {
    const int nElements = GetNumElements();
    
    // Get the current pose
    double Rot[3][3], tvec[3];
    GetPose().GetPose(Rot, tvec);
      
    // Loop over elements. Sum the errors
#pragma omp parallel default(shared)
    {
      // Access thread-local variables
      auto& ws = GetLocalWorkspace();
      auto* Y = ws.v1;
      auto* X = ws.v2;
      auto& psi = ws.psi;
      auto& thr_Err = ws.Err;
      thr_Err = 0.;
      std::vector<double> Qpts, Qwts;
      
#pragma omp for
      for(int e=0; e<nElements; ++e)
	{
	  GetIntegrationPointsAndWeights(e, Qpts, Qwts);
	  const int nQuads = static_cast<int>(Qwts.size());
	  for(int q=0; q<nQuads; ++q)
	    {
	      // xyz coordinates of this quadrature point
	      X = &Qpts[3*q];
	      
	      // This point in the current configuration
	      for(int i=0; i<3; ++i)
		{
		  Y[i] = tvec[i];
		  for(int j=0; j<3; ++j)
		    Y[i] += Rot[i][j]*X[j];
		}

	      // Implicit function value at this point
	      GetImplicitFunction(Y, psi);
	      
	      // Update the error on this thread
	      thr_Err += 0.5*Qwts[q]*psi*psi;
	    }
	}
    }

    // Total error = sum of thread-wise errors
    double Err = 0.;
    for(auto& ws:WrkSpc)
      Err += ws.Err;
    return Err;
  }


    // Compute the residual 
  void Mesh2SurfPoseMatcher::
  GetLinearizations(double* res, double* stiff) const
  {
    const int nElements = GetNumElements();
      
    // Get the current pose
    double Rot[3][3], tvec[3];
    GetPose().GetPose(Rot, tvec);
      
    // Convenience array for computing cross products
    const int next[] = {1,2,0};
    const int nnext[] = {2,0,1};
      
#pragma omp parallel default(shared)
    {
      // Access thread-local variables for residual evaluation
      auto& ws = GetLocalWorkspace();
      auto* W = ws.v1;
      auto* Y = ws.v2;
      auto* dpsi_cross_W = ws.v3;
      auto& psi = ws.psi;
      auto* dpsi = ws.dpsi;
      auto* H = ws.d2psi;
      auto* rVec = ws.rVec;
      auto* kMat = ws.kMat;
      auto* skW = ws.M1;
      auto* skdpsi = ws.M2;
      auto* skW_H = ws.M3;
      auto* skdpsi_skW = ws.M4;
      auto* skW_H_skW = ws.M5;
      std::vector<double> Qwts, Qpts;
      
      for(int i=0; i<6; ++i)
	{
	  rVec[i] = 0.;
	  for(int j=0; j<6; ++j)
	    kMat[i][j] = 0.;
	}
	
#pragma omp for
      for(int e=0; e<nElements; ++e)
	{
	  GetIntegrationPointsAndWeights(e, Qpts, Qwts);
	  const int nQuad = static_cast<int>(Qwts.size());
	  
	  for(int q=0; q<nQuad; ++q)
	    {
	      // This integration point
	      const auto* X = &Qpts[3*q];
	      
	      // This quadrature point in the current configuration
	      for(int i=0; i<3; ++i)
		{
		  W[i] = 0.;
		  for(int j=0; j<3; ++j)
		    W[i] += Rot[i][j]*X[j];
		  Y[i] = W[i] + tvec[i];
		}
	      
	      // Evaluate implicit function, derivative and hessian
	      GetImplicitFunction(Y, psi, dpsi, H);
	      
	      // dpsi_cross_W
	      for(int i=0; i<3; ++i)
		dpsi_cross_W[i] = dpsi[next[i]]*W[nnext[i]]-dpsi[nnext[i]]*W[next[i]];
	      
	      // Skew(W)
	      Pose::HodgeStar(W, skW);
	      
	      // Skew(dpsi)
	      Pose::HodgeStar(dpsi, skdpsi);
	      
	      // Intermediate matrices
	      for(int i=0; i<3; ++i)
		for(int j=0; j<3; ++j)
		  {
		    skW_H[i][j] = 0.;
		    skdpsi_skW[i][j] = 0.;
		    skW_H_skW[i][j] = 0.;
		    for(int k=0; k<3; ++k)
		      {
			skW_H[i][j] += skW[i][k]*H[k][j];
			skdpsi_skW[i][j] += skdpsi[i][k]*skW[k][j];
			for(int L=0; L<3; ++L)
			  skW_H_skW[i][j] += skW[i][k]*H[k][L]*skW[L][j];
		      }
		  }
	      
	      // Update the residue on this thread
	      // 1st 3 rows -> stationarity of translations: psi*dpsi
	      // next 3 rows -> stationarity of rotations: -(psi*dpsi) \times W
	      for(int i=0; i<3; ++i)
		{
		  rVec[i] += Qwts[q]*(psi*dpsi[i]);
		  rVec[3+i] -= Qwts[q]*(psi*dpsi_cross_W[i]);
		}
	      
	      // Update the stiffness on this thread
	      // Ktt: dpsi\otimes dpsi + psi H
	      // Ktr: -dpsi\otimes (dpsi_cross_W) + psi (skW H)^t
	      // Krt: -dpsi_cross_W\otimes dpsi + psi skW H
	      // Krr: (dpsi_cross_W)\otimes (dpsi_cross_W) - psi skW_H_skW + psi skdpsi_skW.
	      for(int i=0; i<3; ++i)
		for(int j=0; j<3; ++j)
		  {
		    kMat[i][j] += Qwts[q]*(dpsi[i]*dpsi[j] + psi*H[i][j]); // Ktt
		    kMat[i][j+3] += Qwts[q]*(-dpsi[i]*dpsi_cross_W[j] + psi*skW_H[j][i]); // Ktr
		    kMat[i+3][j] += Qwts[q]*(-dpsi_cross_W[i]*dpsi[j] + psi*skW_H[i][j]); // Krt
		    kMat[i+3][j+3] += Qwts[q]*(dpsi_cross_W[i]*dpsi_cross_W[j] - psi*skW_H_skW[i][j] + psi*skdpsi_skW[i][j]); // Krr
		  }
	    }
	}
    }
    
    // Collect contributions from each thread
    for(int i=0; i<6; ++i)
      {
	res[i] = 0.;
	for(int j=0; j<6; ++j)
	  stiff[6*i+j] = 0.;
      }
    for(auto& ws:WrkSpc)
      for(int i=0; i<6; ++i)
	{
	  res[i] += ws.rVec[i];
	  for(int j=0; j<6; ++j)
	    stiff[6*i+j] += ws.kMat[i][j];
	}
    
    return;
  } 
  
}
