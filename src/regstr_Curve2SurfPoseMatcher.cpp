// Sriramajayam

#include <regstr_Curve2SurfPoseMatcher.h>
#include <cmath>

namespace regstr
{
  // Compute integration points and weights for a given element
  void Curve2SurfPoseMatcher::GetIntegrationPointsAndWeights(const int elm,
							     std::vector<double>& Qpts,
							     std::vector<double>& Qwts) const
  {
    const int SPD = 3;
    const int nQuad = 2; // 2 point rule
    if(static_cast<int>(Qpts.size())<SPD*nQuad) Qpts.resize(SPD*nQuad);
    if(static_cast<int>(Qwts.size())<nQuad) Qwts.resize(nQuad);
    const int a = connectivity[2*elm+0]-1;
    const int b = connectivity[2*elm+1]-1;
    const double* A = &xyzcoords[SPD*a];
    const double* B = &xyzcoords[SPD*b];
    const double len = std::sqrt( (A[0]-B[0])*(A[0]-B[0]) + (A[1]-B[1])*(A[1]-B[1]) + (A[2]-B[2])*(A[2]-B[2]) );

    // Integration points
    double lambda[] = {0.5*(1.+1./std::sqrt(3.)), 0.5*(1.-1./std::sqrt(3.))};
    for(int q=0; q<nQuad; ++q)
      for(int k=0; k<SPD; ++k)
	Qpts[SPD*q+k] = lambda[q]*A[k] + (1.-lambda[q])*B[k];
    Qwts[0] = len/2.;
    Qwts[1] = len/2.;

    return;
  }

  // Returns the number of elements
 int Curve2SurfPoseMatcher::GetNumElements() const
  { return static_cast<int>(connectivity.size()/2); }
}
