// Sriramajayam

#ifndef REGSTR_POSE_H
#define REGSTR_POSE_H

namespace regstr
{
  // Helper class to save and update a pose
  // consisting of a rotation and a translation
  // The class is not thread safe
  class Pose
  {
  public:
    //! Default constructor.
    //! Sets rotation to the identity and translation to zero
    Pose();

    //! Copy constructor: copy the state
    Pose(const Pose&);

    //! Assignment. Copy the state
    Pose& operator=(const Pose&);

    //! Destructor- nothing to do
    inline virtual ~Pose() {}

    //! Set the pose
    void SetPose(const double R[][3], const double* t);

    //! Returns the pose
    void GetPose(double R[][3], double* t) const;
      
    //! Update point
    void Update(const double* theta, const double* dt);

    //! Compute the skew matrix for a given axial vector
    static void HodgeStar(const double* theta, double skw[][3]);

    //! Compute the exponential map for a given skew matrix
    static void ExpSO3(const double skw[][3], double Mat[][3]);

    //! Compute the determinant of a matrix
    static double Det(const double Mat[][3]);
    
  private:
    double Rot[3][3]; //!< Rotation tensor
    double tvec[3]; //!< Translation vector
    mutable double skw[3][3]; //!< For intermediate calcs
    mutable double expMat[3][3]; //!< For intermediate calcs
    mutable double tMat[3][3]; //!< For intermediate calcs
  };
  
}

#endif
