// Sriramajayam

#ifndef REGSTR_SURF2LEVEL_SURF_POSE_MATCHER_H
#define REGSTR_SURF2LEVEL_SURF_POSE_MATCHER_H

#include <regstr_Surf2SurfPoseMatcher.h>
#include <regstr_PC2LevelSurfPoseMatcher.h>
#include <functional>

namespace regstr
{
  //! Class to compute the rigid body transformations for
  //! matching pose of a point cloud to a surface whose
  //! implicit function has been provided
  class Surf2LevelSurfPoseMatcher: public Surf2SurfPoseMatcher
  {
  public:
    //! Constructor
    //! \param[in] nmaxthreads Max number of threads. Defaulted to 1.
    //! \param[in] func Level set function
    //! \param[in] usrparams User defined parameters to pass to the level set function. Defaulted to null
    inline Surf2LevelSurfPoseMatcher(const std::vector<double>& coords,
				      const std::vector<int>& conn,
				      ImplicitFunction func,
				      void* usrparams=nullptr, const int nmaxthreads=1)
      :Surf2SurfPoseMatcher(coords, conn, nmaxthreads),
      LSFunc(func),
      fparams(usrparams) {}

    //! Destructor
    inline ~Surf2LevelSurfPoseMatcher() {}

    //! Copy constructor
    inline Surf2LevelSurfPoseMatcher(const Surf2LevelSurfPoseMatcher& obj)
      :Surf2SurfPoseMatcher(obj), LSFunc(obj.LSFunc), fparams(obj.fparams) {}

    //! Disable assignment
    Surf2LevelSurfPoseMatcher& operator=(const Surf2LevelSurfPoseMatcher&) = delete;

    //! Main functionality: compute implicit function and derivatives
    inline virtual void GetImplicitFunction(const double* X, double& psi,
					    double* dpsi=nullptr,
					    double d2psi[][3]=nullptr) const override
    { LSFunc(X, psi, dpsi, d2psi, fparams); }

  private:
    ImplicitFunction LSFunc;
    void* fparams;
  };
}

#endif
