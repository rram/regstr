// Sriramajayam

#ifndef REGSTR_SURF2MAX_ENT_SURF_POSE_MATCHER_H
#define REGSTR_SURF2MAX_ENT_SURF_POSE_MATCHER_H

#include <regstr_Surf2SurfPoseMatcher.h>
#include <regstr_PC2MaxEntSurfPoseMatcher.h>

namespace regstr
{
  //! Helper struct for max-ent function evaluations
  using Surf2MaxEntSurfPoseMatcherWorkspace = PC2MaxEntSurfPoseMatcherWorkspace;
  
  //! Class to compute the rigid body transformations for
  //! matching pose of a point cloud to a surface that is
  //! given implicitly using max-ent functions
  class Surf2MaxEntSurfPoseMatcher: public Surf2SurfPoseMatcher
  {
  public:
    //! Constructor
    //! \param[in] nmaxthreads Max number of threads. Defaulted to 1.
    //! \param[in] surf Max-ent surface fit
    //! \param[in] usrparams User defined parameters to pass to the level set function. Defaulted to null
    Surf2MaxEntSurfPoseMatcher(const std::vector<double>& coords,
			       const std::vector<int>& conn, 
			       const mx::MaxEntSSD<3>& surf,
			       const int nmaxthreads=1);
    
    //! Destructor, does nothing
    inline virtual ~Surf2MaxEntSurfPoseMatcher() {}
    
    //! Copy constructor
    Surf2MaxEntSurfPoseMatcher(const Surf2MaxEntSurfPoseMatcher&);

    //! Disable assignment
    Surf2MaxEntSurfPoseMatcher& operator=(const Surf2MaxEntSurfPoseMatcher&)=delete;

    //! Main functionality: compute the implicit function and derivatives
    //! Is thread safe.
    virtual void GetImplicitFunction(const double* X, double& psi,
				     double* dpsi=nullptr,
				     double d2psi[][3]=nullptr) const override;

  private:
    //! Access the local workspace for function evaluations
    Surf2MaxEntSurfPoseMatcherWorkspace& GetLocalWorkspace() const;

    const mx::MaxEntSSD<3>& SurfFit; //!< Reference to the max-ent surface
    mutable std::vector<Surf2MaxEntSurfPoseMatcherWorkspace> MaxEntWrkSpc;
  };
    
}

#endif
