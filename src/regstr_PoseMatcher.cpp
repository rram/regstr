// Sriramajayam

#include <regstr_PoseMatcher.h>
#include <cassert>
#include <cmath>

namespace regstr
{
  //                PoseMatcherWorkspace
  // --------------------------------------------------
  // Constructor: allocate memory for gsl structure
  PoseMatcherWorkspace::PoseMatcherWorkspace()
  {
    // Workspace for matrix inversion
    kMat = gsl_matrix_alloc(6,6);
    rVec = gsl_vector_alloc(6);
    inc = gsl_vector_alloc(6);
    perm = gsl_permutation_alloc(6);

    // For eigenvalue & eigencvector calculations
    eigWrkSpc = gsl_eigen_nonsymmv_alloc(6);
    eigVals = gsl_vector_complex_alloc(6);
    eigVecs = gsl_matrix_complex_alloc(6,6);
  }
    
  // Destructor: deallocate memory for gsl structures
  PoseMatcherWorkspace::~PoseMatcherWorkspace()
  {
    gsl_matrix_free(kMat);
    gsl_vector_free(rVec);
    gsl_vector_free(inc);
    gsl_permutation_free(perm);

    gsl_eigen_nonsymmv_free(eigWrkSpc);
    gsl_vector_complex_free(eigVals);
    gsl_matrix_complex_free(eigVecs);
  }
  // --------------------------------------------------


  //                PoseMatcher
  // --------------------------------------------------
  // Main functionality: Iterate
  // Returns the norm of the residual and the solution increment computed
  void PoseMatcher::Iterate(const double factor, double& solNorm, double& resNorm)
  {
    // Aliases
    auto* rVec = ws.rVec;
    auto* kMat = ws.kMat;
    auto* perm = ws.perm;
    auto* inc = ws.inc;
	
    // Compute the linearizations at the current state
    GetLinearizations(rVec->data, kMat->data);

    // Compute the LU decomposition of kMat
    int psign; // sign of the permutation
    gsl_linalg_LU_decomp(kMat, perm, &psign);

    // Solve the system [kMat] inc = [rVec]
    gsl_linalg_LU_solve(kMat, perm, rVec, inc);

    // Scale the solution by -factor
    gsl_vector_scale(inc, -factor);

    // Update the pose based on the current increment
    pose.Update(&inc->data[3], &inc->data[0]);

    // Compute the residual and solution norms
    resNorm = solNorm = 0.;
    for(int i=0; i<6; ++i)
      {
	resNorm += rVec->data[i]*rVec->data[i];
	solNorm += inc->data[i]*inc->data[i];
      }
    resNorm = sqrt(resNorm);
    solNorm = sqrt(solNorm);

    // Finished
    return;
  }


  // Main functionality: Iterate with specific dofs set to zero.
  void PoseMatcher::Iterate(const double factor, const int nzerodofs, const int* zerodofs,
			    double& solNorm, double& resNorm)
  {
    assert((nzerodofs>0 && zerodofs!=nullptr) && "regstr::PoseMatcher::Iterate- Unexpected dof specs");

    // Aliases
    auto* rVec = ws.rVec;
    auto* kMat = ws.kMat;
    auto* perm = ws.perm;
    auto* inc = ws.inc;
      
    // Compute the linearizations at the current state
    GetLinearizations(rVec->data, kMat->data);

    // Set zero bcs by modifying corresponding rows of the matrix and residual
    for(int i=0; i<nzerodofs; ++i)
      {
	int zdof = zerodofs[i];
	assert((zdof>=0 && zdof<6) && "regstr::PoseMatcher::Iterate- Unexpected dof number");
	for(int j=0; j<6; ++j)
	  kMat->data[6*zdof+j] = 0.;
	kMat->data[6*zdof+zdof] = 1.;
	rVec->data[zdof] = 0.;
      }
	  
    // Compute the LU decomposition of kMat
    int psign; // sign of the permutation
    gsl_linalg_LU_decomp(kMat, perm, &psign);

    // Solve the system [kMat] inc = [rVec]
    gsl_linalg_LU_solve(kMat, perm, rVec, inc);

    // Check that the values of dofs with zero values has been enforced
    for(int i=0; i<nzerodofs; ++i)
      assert(std::abs(inc->data[zerodofs[i]])<1.e-6 && "regstr::PoseMatcher::Iterate- Dof value not enforced");
      
    // Scale the solution by -factor
    gsl_vector_scale(inc, -factor);

    // Update the pose based on the current increment
    pose.Update(&inc->data[3], &inc->data[0]);
      
    // Compute the residual and solution norms
    resNorm = solNorm = 0.;
    for(int i=0; i<6; ++i)
      {
	resNorm += rVec->data[i]*rVec->data[i];
	solNorm += inc->data[i]*inc->data[i];
      }
    resNorm = sqrt(resNorm);
    solNorm = sqrt(solNorm);
      
    // Finished
    return;
  }
      


  // Computes the eigenvalues at the current pose
  void PoseMatcher::GetEigenvalues(double* vals) const
  {
    assert(vals!=nullptr &&
	   "regstr::PoseMatcher::GetEigenvalues- non-null pointer required for eigenvalues");
      
    // Aliases
    auto* rVec = ws.rVec;
    auto* kMat = ws.kMat;
    auto* eigWrkSpc = ws.eigWrkSpc;
    auto* eigVals = ws.eigVals;
    auto* eigVecs = ws.eigVecs;
      
    // Compute the linearizations at the current state
    GetLinearizations(rVec->data, kMat->data);

    // Compute the eigenvalues
    gsl_eigen_nonsymmv(kMat, eigVals, eigVecs, eigWrkSpc);

    // TODO check the return code

    // Save the eigenvalues
    for(int i=0; i<6; ++i)
      for(int k=0; k<2; ++k)
	vals[2*i+k] = eigVals->data[2*i+k];

    return;
  }
    
}
